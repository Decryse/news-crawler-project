create table news
(
    title varchar,
    content varchar,
    website varchar,
    image varchar,
    author varchar,
    tags varchar
);