package com.holovenkoml.crawler.websites.template;

import java.util.Set;

public interface TakeLinksOfNewsFromSourcesStrategy {
    Set<String> getLinks(int upperBound);
}
