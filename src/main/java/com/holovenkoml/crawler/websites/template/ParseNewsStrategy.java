package com.holovenkoml.crawler.websites.template;

import com.holovenkoml.crawler.application.domainobject.News;

public interface ParseNewsStrategy {
    News parse(String url);
}
