package com.holovenkoml.crawler.websites.template;

import org.jsoup.nodes.Document;

public interface ConnectionProvider {
    Document provideConnectionToTheNextPage();
}
