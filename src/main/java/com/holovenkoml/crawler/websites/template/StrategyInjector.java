package com.holovenkoml.crawler.websites.template;

public interface StrategyInjector {
    void inject();
}
