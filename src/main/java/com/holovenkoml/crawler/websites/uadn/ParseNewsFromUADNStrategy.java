package com.holovenkoml.crawler.websites.uadn;

import com.holovenkoml.crawler.application.domainobject.News;
import com.holovenkoml.crawler.websites.template.ParseNewsStrategy;
import com.holovenkoml.crawler.application.utils.ConnectionUtils;
import com.holovenkoml.crawler.application.utils.ParsingUtils;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class ParseNewsFromUADNStrategy implements ParseNewsStrategy {
    @Override
    public News parse(String url) {
        Document newsPage = ConnectionUtils.getConnection(url);

        String title = ParsingUtils.getTextBySelector(newsPage, ".entry-title");
        String text = ParsingUtils.getTextOfThePage(newsPage, "p");
        String source = ParsingUtils.getSource(url);
        String mainImage = ParsingUtils.getLinkBySelectorAndAttribute(newsPage, ".wp-post-image.size-large.attachment-large", "src");
        String publisher = ParsingUtils.getLinkBySelectorAndAttribute(newsPage, ".author.vcard.posted-by > .n.fn.url", "href");
        Set<String> tags = ParsingUtils.getTags(newsPage, ".cat-links > a");

        return new News.Builder(source, title, text)
                .withMainImage(mainImage)
                .withPublisher(publisher)
                .withTags(tags)
                .build();
    }
}
