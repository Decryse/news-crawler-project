package com.holovenkoml.crawler.websites.uadn;

import com.holovenkoml.crawler.websites.template.TakeLinksOfNewsFromSourcesStrategy;
import com.holovenkoml.crawler.websites.template.ParseNewsStrategy;
import com.holovenkoml.crawler.websites.template.StrategyInjector;
import com.holovenkoml.crawler.application.strategy.StrategyManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class UADNStrategyInjector implements StrategyInjector {

    private final StrategyManager<ParseNewsStrategy> parseNewsStrategyManager;
    private final StrategyManager<TakeLinksOfNewsFromSourcesStrategy> takeLinksStrategyManager;
    private final ParseNewsStrategy parseNewsStrategy;
    private final TakeLinksOfNewsFromSourcesStrategy takeLinksOfNewsFromSourcesStrategy;

    @Autowired
    public UADNStrategyInjector(StrategyManager<ParseNewsStrategy> parseNewsStrategyManager,
                                StrategyManager<TakeLinksOfNewsFromSourcesStrategy> takeLinksStrategyManager,
                                @Qualifier("parseNewsFromUADNStrategy") ParseNewsStrategy parseNewsStrategy,
                                @Qualifier("takeLinksFromUADNStrategy")TakeLinksOfNewsFromSourcesStrategy takeLinksOfNewsFromSourcesStrategy) {
        this.parseNewsStrategyManager = parseNewsStrategyManager;
        this.takeLinksStrategyManager = takeLinksStrategyManager;
        this.parseNewsStrategy = parseNewsStrategy;
        this.takeLinksOfNewsFromSourcesStrategy = takeLinksOfNewsFromSourcesStrategy;
    }

    @Override
    public void inject() {
        parseNewsStrategyManager.addStrategy("https://www.uadn.net/", parseNewsStrategy);
        takeLinksStrategyManager.addStrategy("https://www.uadn.net/", takeLinksOfNewsFromSourcesStrategy);
    }
}
