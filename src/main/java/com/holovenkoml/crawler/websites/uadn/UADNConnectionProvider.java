package com.holovenkoml.crawler.websites.uadn;

import com.holovenkoml.crawler.application.utils.ConnectionUtils;
import com.holovenkoml.crawler.websites.template.ConnectionProvider;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;

@Component("uadnConnectionProvider")
public class UADNConnectionProvider implements ConnectionProvider {

    private final AtomicInteger pageNumber = new AtomicInteger(1);

    @Override
    public Document provideConnectionToTheNextPage() {
        return ConnectionUtils.getConnection("https://www.uadn.net/page/" + pageNumber.getAndIncrement() + "/");
    }
}
