package com.holovenkoml.crawler.websites.itworldcanada;

import com.holovenkoml.crawler.application.domainobject.News;
import com.holovenkoml.crawler.application.utils.ConnectionUtils;
import com.holovenkoml.crawler.application.utils.ParsingUtils;
import com.holovenkoml.crawler.websites.template.ParseNewsStrategy;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class ParseNewsFromItWorldCanadaStrategy implements ParseNewsStrategy {
    @Override
    public News parse(String url) {
        Document newsPage = ConnectionUtils.getConnection(url);

        String title = ParsingUtils.getTextBySelector(newsPage, ".article-title > a");
        String text = ParsingUtils.getTextOfThePage(newsPage, ".entry-content > p");
        String source = ParsingUtils.getSource(url);
        String mainImage = ParsingUtils.getLinkBySelectorAndAttribute(newsPage, ".wp-post-image.size-large.attachment-large", "src");
        String publisher = ParsingUtils.getLinkBySelectorAndAttribute(newsPage, ".author-name > a", "href");
        Set<String> tags = ParsingUtils.getTags(newsPage, ".red.category-title > a");

        return new News.Builder(source, title, text)
                .withMainImage(mainImage)
                .withPublisher(publisher)
                .withTags(tags)
                .build();
    }
}
