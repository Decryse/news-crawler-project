package com.holovenkoml.crawler.websites.itworldcanada;

import com.holovenkoml.crawler.application.strategy.StrategyManager;
import com.holovenkoml.crawler.websites.template.ParseNewsStrategy;
import com.holovenkoml.crawler.websites.template.StrategyInjector;
import com.holovenkoml.crawler.websites.template.TakeLinksOfNewsFromSourcesStrategy;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class ItWorldCanadaStrategyInjector implements StrategyInjector {

    private final StrategyManager<ParseNewsStrategy> parseNewsStrategyManager;
    private final StrategyManager<TakeLinksOfNewsFromSourcesStrategy> takeLinksStrategyManager;
    private final ParseNewsStrategy parseNewsStrategy;
    private final TakeLinksOfNewsFromSourcesStrategy takeLinksOfNewsFromSourcesStrategy;

    public ItWorldCanadaStrategyInjector(StrategyManager<ParseNewsStrategy> parseNewsStrategyManager,
                                         StrategyManager<TakeLinksOfNewsFromSourcesStrategy> takeLinksStrategyManager,
                                         @Qualifier("parseNewsFromItWorldCanadaStrategy") ParseNewsStrategy parseNewsStrategy,
                                         @Qualifier("takeLinksFromItWorldCanadaStrategy")TakeLinksOfNewsFromSourcesStrategy takeLinksOfNewsFromSourcesStrategy) {
        this.parseNewsStrategyManager = parseNewsStrategyManager;
        this.takeLinksStrategyManager = takeLinksStrategyManager;
        this.parseNewsStrategy = parseNewsStrategy;
        this.takeLinksOfNewsFromSourcesStrategy = takeLinksOfNewsFromSourcesStrategy;
    }

    @Override
    public void inject() {
        parseNewsStrategyManager.addStrategy("https://www.itworldcanada.com/", parseNewsStrategy);
        takeLinksStrategyManager.addStrategy("https://www.itworldcanada.com/", takeLinksOfNewsFromSourcesStrategy);
    }
}
