package com.holovenkoml.crawler.websites.itworldcanada;

import com.holovenkoml.crawler.application.utils.ConnectionUtils;
import com.holovenkoml.crawler.websites.template.ConnectionProvider;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;

@Component
public class ItWorldCanadaConnectionProvider implements ConnectionProvider {

    private final AtomicInteger page = new AtomicInteger(2);

    @Override
    public Document provideConnectionToTheNextPage() {
        return ConnectionUtils.getConnection("https://www.itworldcanada.com/category/software/page/" + page.getAndIncrement());
    }
}
