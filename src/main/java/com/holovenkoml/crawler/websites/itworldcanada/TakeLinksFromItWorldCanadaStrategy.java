package com.holovenkoml.crawler.websites.itworldcanada;

import com.holovenkoml.crawler.websites.template.ConnectionProvider;
import com.holovenkoml.crawler.websites.template.TakeLinksOfNewsFromSourcesStrategy;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class TakeLinksFromItWorldCanadaStrategy implements TakeLinksOfNewsFromSourcesStrategy {
    private static final String CSS_SELECTOR_FOR_LINKS_TO_THE_NEWS = ".article-title > a";

    private final ConnectionProvider connectionProvider;

    @Autowired
    public TakeLinksFromItWorldCanadaStrategy(@Qualifier("itWorldCanadaConnectionProvider") ConnectionProvider connectionProvider){
        this.connectionProvider = connectionProvider;
    }

    @Override
    public Set<String> getLinks(int upperBound) {
        Set<String> linksToTheNews = new HashSet<>();
        while(linksToTheNews.size() < upperBound){
            Document document = connectionProvider.provideConnectionToTheNextPage();
            Elements links = document.select(CSS_SELECTOR_FOR_LINKS_TO_THE_NEWS);
            linksToTheNews.addAll(links.stream()
                    .map(link -> link.attr("href"))
                    .limit(upperBound - linksToTheNews.size())
                    .collect(Collectors.toSet()));
        }
        return linksToTheNews;
    }
}
