package com.holovenkoml.crawler.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.holovenkoml.crawler")
public class JavaConfig {
}
