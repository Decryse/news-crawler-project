package com.holovenkoml.crawler.application.repositories;

import com.holovenkoml.crawler.application.domainobject.News;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Component
public class NewsRepositoryImpl implements NewsRepository{

    private final Set<News> parsedNews;

    public NewsRepositoryImpl(){
        this.parsedNews = Collections.synchronizedSet(new HashSet<>());
    }

    @Override
    public Set<News> getParsedNews() {
        return parsedNews;
    }

    @Override
    public void addParsedNews(News news){
        parsedNews.add(news);
    }
}
