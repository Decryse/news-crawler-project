package com.holovenkoml.crawler.application.repositories;

import com.holovenkoml.crawler.application.db.DataBaseConnectionManager;
import com.holovenkoml.crawler.application.domainobject.News;
import com.holovenkoml.crawler.application.printing.Printer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Set;

@Component
public class NewsDataBaseRepository implements DataBaseRepository {

    private final NewsRepository newsRepository;
    private final DataBaseConnectionManager dataBaseConnectionManager;
    private final Printer printer;

    @Autowired
    public NewsDataBaseRepository(NewsRepository newsRepository,
                                  DataBaseConnectionManager dataBaseConnectionManager,
                                  Printer printer){
        this.newsRepository = newsRepository;
        this.dataBaseConnectionManager = dataBaseConnectionManager;
        this.printer = printer;
    }

    public void writeToDataBase(){
        printer.print("Writing to data base...");
        Set<News> newsSet = newsRepository.getParsedNews();
        try(
                Connection connection = dataBaseConnectionManager.getDataSource().getConnection();
                PreparedStatement statement = connection.prepareStatement(
                        "INSERT INTO news (title, content, website, image, author, tags)" +
                                "VALUES (?, ?, ?, ?, ?, ?);"
                )
        ) {
            for (News news : newsSet){
                statement.setString(1, news.getTitle());
                statement.setString(2, news.getText());
                statement.setString(3, news.getSourceWebsite());
                statement.setString(4, news.getMainImage());
                statement.setString(5, news.getPublisher());
                statement.setString(6, news.getTags().toString());
                statement.addBatch();
            }
            statement.executeBatch();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }
}
