package com.holovenkoml.crawler.application.repositories;

import com.holovenkoml.crawler.application.domainobject.News;

import java.util.Set;

public interface NewsRepository {
    Set<News> getParsedNews();
    void addParsedNews(News news);
}
