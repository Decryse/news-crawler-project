package com.holovenkoml.crawler.application.repositories;

import java.util.Set;
import java.util.concurrent.BlockingQueue;

public interface LinksRepository {
    void putUnparsedLink(String link);
    BlockingQueue<String> getUnparsedLink();
    Set<String> getSourceWebsites();
    Set<String> getAllLinksToTheWebsites();
    void addLinksToTheNews(Set<String> linksToTheNews);
}
