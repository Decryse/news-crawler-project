package com.holovenkoml.crawler.application.repositories;

import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.stream.Collectors;

@Component
public class LinksRepositoryImpl implements LinksRepository{

    private final Set<String> linksToTheNews;
    private final BlockingQueue<String> unparsedLinks;

    public LinksRepositoryImpl(){
        this.linksToTheNews = Collections.synchronizedSet(new HashSet<>());
        this.unparsedLinks = new ArrayBlockingQueue<>(10);
    }

    @Override
    public void putUnparsedLink(String link){
        try {
            unparsedLinks.put(link);
        } catch (InterruptedException e) {
            e.printStackTrace();
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public BlockingQueue<String> getUnparsedLink(){
        return unparsedLinks;
    }

    @Override
    public Set<String> getSourceWebsites() {
        try(BufferedReader reader = new BufferedReader(new
                InputStreamReader(Objects.requireNonNull(LinksRepositoryImpl.class.getClassLoader().getResourceAsStream("news-websites.txt"))))){
            return reader
                    .lines()
                    .collect(Collectors.toSet());
        }catch (IOException x){
            return Set.of();
        }
    }

    @Override
    public Set<String> getAllLinksToTheWebsites(){
        return linksToTheNews;
    }

    @Override
    public void addLinksToTheNews(Set<String> linksToTheNews){
        this.linksToTheNews.addAll(linksToTheNews);
    }
}
