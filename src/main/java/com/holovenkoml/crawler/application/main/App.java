package com.holovenkoml.crawler.application.main;

import com.holovenkoml.crawler.config.JavaConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {

    public static void main(String[] args){
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(JavaConfig.class);

        WorkManager workManager = new WorkManager(600, applicationContext);
        workManager.startParsing();
        System.out.println("Program successfully finished");
    }
}
