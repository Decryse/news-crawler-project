package com.holovenkoml.crawler.application.main;

import com.holovenkoml.crawler.application.repositories.NewsDataBaseRepository;
import com.holovenkoml.crawler.application.linkshandling.GeneralLinksHandler;
import com.holovenkoml.crawler.application.linkshandling.UnparsedLinksHandler;
import com.holovenkoml.crawler.application.newsparsing.WebPagesParser;
import com.holovenkoml.crawler.application.repositories.DataBaseRepository;
import com.holovenkoml.crawler.application.newsparsing.Parser;
import com.holovenkoml.crawler.application.utils.ExecutorUtils;
import org.springframework.context.ApplicationContext;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class WorkManager {

    private final int upperBound;
    private final ApplicationContext applicationContext;

    public WorkManager(int upperBound, ApplicationContext applicationContext){
        this.upperBound = upperBound;
        this.applicationContext = applicationContext;
    }

    public void startParsing(){
        lookForLinksIntoSources();
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        executorService.execute(this::handleUnparsedLinks);
        parseNews();
        writeToDataBase();
        ExecutorUtils.shutDownExecutorServiceCorrectly(executorService);
    }

    private void handleUnparsedLinks(){
        UnparsedLinksHandler unparsedLinksHandler = applicationContext.getBean("unparsedLinksHandler", UnparsedLinksHandler.class);
        unparsedLinksHandler.handleLinks();
    }

    private void writeToDataBase() {
        DataBaseRepository dataBaseRepository = applicationContext.getBean("newsDataBaseRepository", NewsDataBaseRepository.class);
        dataBaseRepository.writeToDataBase();
    }

    private void parseNews() {
        Parser parser = applicationContext.getBean("webPagesParser", WebPagesParser.class);
        parser.parse();
    }

    private void lookForLinksIntoSources() {
        GeneralLinksHandler generalLinksHandler = applicationContext.getBean("generalLinksHandler", GeneralLinksHandler.class);
        generalLinksHandler.handleLinks(upperBound);
    }
}
