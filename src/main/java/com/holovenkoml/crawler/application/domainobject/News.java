package com.holovenkoml.crawler.application.domainobject;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class News {

    private final String sourceWebsite;
    private final String title;
    private final String text;
    private final String mainImage;
    private final String publisher;
    private final Set<String> tags;

    private News(Builder builder) {
        this.sourceWebsite = builder.sourceWebsite;
        this.title = builder.title;
        this.text = builder.text;
        this.mainImage = builder.mainImage;
        this.publisher = builder.publisher;
        this.tags = builder.tags;
    }

    public String getSourceWebsite() {
        return sourceWebsite;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public String getMainImage() {
        return mainImage;
    }

    public String getPublisher() {
        return publisher;
    }

    public Set<String> getTags() {
        return tags;
    }

    public static class Builder{
        private final String sourceWebsite;
        private final String title;
        private final String text;
        private String mainImage = "https://edm.com/.image/t_share/MTc0NjYyNjI0ODg4MjM1Mzg2/science-of-earworms-explain-why-never-gonna-give-you-up-is-stuck-in-our-heads-30-years-later.png";
        private String publisher = "Publisher not found";
        private Set<String> tags = new HashSet<>();

        public Builder(String sourceWebsite, String title, String text){
            this.sourceWebsite = sourceWebsite;
            this.title = title;
            this.text = text;
        }

        public Builder withMainImage(String imageURL){
            mainImage = imageURL;
            return this;
        }

        public Builder withPublisher(String publisher){
            this.publisher = publisher;
            return this;
        }

        public Builder withTags(Set<String> tags){
            this.tags = tags;
            return this;
        }

        public News build(){
            return new News(this);
        }
    }

    @Override
    public String toString() {
        return "News{" +
                "sourceWebsite='" + sourceWebsite + '\'' + "\n" +
                ", title='" + title + '\'' + "\n" +
                ", text='" + text + '\'' + "\n" +
                ", mainImage='" + mainImage + '\'' + "\n" +
                ", publisher='" + publisher + '\'' + "\n" +
                ", tags=" + tags + "\n" +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        News news = (News) o;
        return Objects.equals(sourceWebsite, news.sourceWebsite) && Objects.equals(title, news.title) && Objects.equals(text, news.text) && Objects.equals(mainImage, news.mainImage) && Objects.equals(publisher, news.publisher) && Objects.equals(tags, news.tags);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sourceWebsite, title, text, mainImage, publisher, tags);
    }
}
