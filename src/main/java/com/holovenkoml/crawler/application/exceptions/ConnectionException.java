package com.holovenkoml.crawler.application.exceptions;

public class ConnectionException extends RuntimeException{

    private final String badURL;

    public ConnectionException(String badURL){
        this.badURL = badURL;
    }

    @Override
    public String getMessage() {
        return "Failed to connect to " + badURL;
    }
}
