package com.holovenkoml.crawler.application.exceptions;

public class ParsingException extends RuntimeException{

    private final String problem;

    public ParsingException(String problem){
        this.problem = problem;
    }

    @Override
    public String getMessage() {
        return "Problems with parsing " + problem;
    }
}
