package com.holovenkoml.crawler.application.exceptions;

public class UnsupportedWebsiteException extends RuntimeException{

    private final String url;

    public UnsupportedWebsiteException(String url){
        this.url = url;
    }

    @Override
    public String getMessage() {
        return "Website " + url + " unsupported in current application version";
    }
}
