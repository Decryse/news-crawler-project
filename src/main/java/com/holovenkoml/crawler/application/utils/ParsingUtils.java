package com.holovenkoml.crawler.application.utils;

import com.holovenkoml.crawler.application.exceptions.ParsingException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Set;
import java.util.stream.Collectors;

public class ParsingUtils {
    private ParsingUtils(){}

    public static String getTextBySelector(Document document, String cssSelector){
        Element element = document.selectFirst(cssSelector);
        if (element != null){
            return element.text();
        }

        throw new ParsingException(cssSelector);
    }

    public static String getLinkBySelectorAndAttribute(Document document, String selector, String attr){
        Element element = document.selectFirst(selector);
        if (element != null){
            return element.attr(attr);
        }

        throw new ParsingException(selector);
    }

    public static String getTextOfThePage(Document document, String cssSelector){
        Elements text = document.select(cssSelector);

        if (text != null){
            StringBuilder content = text.stream()
                    .collect(StringBuilder::new,
                            (stringBuilder, element) -> stringBuilder.append(element.text()).append("\n"),
                            StringBuilder::append);
            return content.toString();
        }

        throw new ParsingException(cssSelector);
    }

    public static String getSource(String url){
        try{
            URI uri = new URI(url);
            String domain = uri.getHost();
            return domain.startsWith("www.") ? domain.substring(4) : domain;
        } catch (URISyntaxException uriSyntaxException){
            throw new ParsingException(url);
        }
    }

    public static Set<String> getTags(Document document, String cssSelector){
        Elements elements = document.select(cssSelector);
        return elements.stream()
                .map(Element::text)
                .collect(Collectors.toSet());
    }
}
