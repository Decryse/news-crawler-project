package com.holovenkoml.crawler.application.utils;

import com.holovenkoml.crawler.application.exceptions.ConnectionException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

public class ConnectionUtils {

    private ConnectionUtils(){}

    public static Document getConnection(String url){
        try {
            return Jsoup.connect(url).get();
        } catch (IOException e) {
            throw new ConnectionException(url);
        }
    }
}
