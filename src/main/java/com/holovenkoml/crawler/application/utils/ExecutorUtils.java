package com.holovenkoml.crawler.application.utils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

public class ExecutorUtils {

    private ExecutorUtils(){}

    public static void shutDownExecutorServiceCorrectly(ExecutorService executorService){
        executorService.shutdown();
        try{
            if (!executorService.awaitTermination(60, TimeUnit.SECONDS)){
                executorService.shutdownNow();
                if (!executorService.awaitTermination(60, TimeUnit.SECONDS)){
                    System.err.println("Pool did not terminate");
                }
            }
        } catch (InterruptedException ie){
            executorService.shutdownNow();
            Thread.currentThread().interrupt();
        }
    }
}
