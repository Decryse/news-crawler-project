package com.holovenkoml.crawler.application.linkshandling;

import com.holovenkoml.crawler.application.printing.Printer;
import com.holovenkoml.crawler.application.repositories.LinksRepository;
import com.holovenkoml.crawler.application.utils.ExecutorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.*;

@Component
public class UnparsedLinksHandler{

    private final LinksRepository linksRepository;
    private final Printer printer;

    @Autowired
    public UnparsedLinksHandler(LinksRepository linksRepository, Printer printer){
        this.linksRepository = linksRepository;
        this.printer = printer;
    }

    public void handleLinks(){
        Set<String> unparsedLinks = new HashSet<>();
        ExecutorService executorService = createSingleThreadExecutorWithDaemonThread();
        executorService.execute(() -> {
            while (true){
                String unparsedLink = getUnparsedLinkWithTimeout();
                if (unparsedLink == null){
                    break;
                } else {
                    unparsedLinks.add(unparsedLink);
                    printWarningMessageEveryThreeLink(unparsedLinks);
                }
            }
        });
        ExecutorUtils.shutDownExecutorServiceCorrectly(executorService);
    }

    private ExecutorService createSingleThreadExecutorWithDaemonThread() {
        return Executors.newSingleThreadExecutor(
                (Runnable r) -> {
                    Thread t = new Thread(r);
                    t.setDaemon(true);
                    return t;
                }
        );
    }

    private void printWarningMessageEveryThreeLink(Set<String> unparsedLinks) {
        if (unparsedLinks.size() % 3 == 0){
            printer.print("Pay attention. There is three failed to parse links in a row");
        }
    }

    private String getUnparsedLinkWithTimeout() {
        String unparsedLink = null;
        try {
            unparsedLink = linksRepository.getUnparsedLink().take();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        return unparsedLink;
    }
}