package com.holovenkoml.crawler.application.linkshandling;

import com.holovenkoml.crawler.application.exceptions.UnsupportedWebsiteException;
import com.holovenkoml.crawler.application.strategy.StrategyManager;
import com.holovenkoml.crawler.websites.template.TakeLinksOfNewsFromSourcesStrategy;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;

public class LinksGrabbingTask implements Callable<Set<String>> {

    private final int upperBound;
    private final String url;
    private final StrategyManager<TakeLinksOfNewsFromSourcesStrategy> strategyManager;

    public LinksGrabbingTask(String url, int upperBound, StrategyManager<TakeLinksOfNewsFromSourcesStrategy> takingLinksStrategyManager){
        this.url = url;
        this.upperBound = upperBound;
        this.strategyManager = takingLinksStrategyManager;
    }

    @Override
    public Set<String> call() {
        Map<String, TakeLinksOfNewsFromSourcesStrategy> strategyMapForTakingLinks = strategyManager.getStrategyMap();
        if(strategyMapForTakingLinks.containsKey(url)){
            return strategyMapForTakingLinks.get(url).getLinks(upperBound);
        }

        throw new UnsupportedWebsiteException(url);
    }
}
