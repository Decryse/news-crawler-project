package com.holovenkoml.crawler.application.linkshandling;

import com.holovenkoml.crawler.application.printing.Printer;
import com.holovenkoml.crawler.application.repositories.LinksRepository;
import com.holovenkoml.crawler.application.strategy.StrategyManager;
import com.holovenkoml.crawler.application.utils.ExecutorUtils;
import com.holovenkoml.crawler.websites.template.TakeLinksOfNewsFromSourcesStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.concurrent.*;

@Component
public class GeneralLinksHandler{

    private final LinksRepository linksRepository;
    private final StrategyManager<TakeLinksOfNewsFromSourcesStrategy> strategyManager;
    private final Printer printer;

    @Autowired
    public GeneralLinksHandler(LinksRepository linksRepository,
                               StrategyManager<TakeLinksOfNewsFromSourcesStrategy> strategyManager, Printer printer){
        this.linksRepository = linksRepository;
        this.strategyManager = strategyManager;
        this.printer = printer;
    }

    public void handleLinks(int upperBound){
        printer.print("Looking for news urls...");
        Set<String> links = linksRepository.getSourceWebsites();
        ExecutorService executor = Executors.newFixedThreadPool(links.size());
        CompletionService<Set<String>> completionService = new ExecutorCompletionService<>(executor);
        submitTasks(links, completionService, upperBound);
        executeFutures(completionService, links.size());
        ExecutorUtils.shutDownExecutorServiceCorrectly(executor);
    }

    private void submitTasks(Set<String> links, CompletionService<Set<String>> completionService, int upperBound){
        links.forEach(link -> {
            Callable<Set<String>> task = new LinksGrabbingTask(link, upperBound, strategyManager);
            completionService.submit(task);
        });
    }

    private void executeFutures(CompletionService<Set<String>> completionService, int amountOfFutures){
        for (int i = 0; i < amountOfFutures; i++){
            try {
                Future<Set<String>> future = completionService.take();
                linksRepository.addLinksToTheNews(future.get());
            } catch (ExecutionException e) {
                System.out.println(e.getMessage());
            } catch (InterruptedException x){
                x.printStackTrace();
                Thread.currentThread().interrupt();
            }

        }
    }
}
