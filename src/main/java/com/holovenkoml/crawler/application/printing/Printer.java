package com.holovenkoml.crawler.application.printing;

public interface Printer {
    void print(String text);
}
