package com.holovenkoml.crawler.application.strategy;

import com.holovenkoml.crawler.websites.template.TakeLinksOfNewsFromSourcesStrategy;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class TakingLinksStrategyManager implements StrategyManager<TakeLinksOfNewsFromSourcesStrategy>{

    private final Map<String, TakeLinksOfNewsFromSourcesStrategy> strategyMapForTakingLinks;

    public TakingLinksStrategyManager(){
        this.strategyMapForTakingLinks = new HashMap<>();
    }

    @Override
    public Map<String, TakeLinksOfNewsFromSourcesStrategy> getStrategyMap() {
        return strategyMapForTakingLinks;
    }

    @Override
    public void addStrategy(String strategyName, TakeLinksOfNewsFromSourcesStrategy strategy) {
        strategyMapForTakingLinks.put(strategyName, strategy);
    }
}
