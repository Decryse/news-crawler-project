package com.holovenkoml.crawler.application.strategy;

import com.holovenkoml.crawler.websites.template.ParseNewsStrategy;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class ParsingNewsStrategyManager implements StrategyManager<ParseNewsStrategy>{

    private final Map<String, ParseNewsStrategy> strategyMapForParsingNews;

    public ParsingNewsStrategyManager(){
        strategyMapForParsingNews = new HashMap<>();
    }

    @Override
    public Map<String, ParseNewsStrategy> getStrategyMap() {
        return strategyMapForParsingNews;
    }

    @Override
    public void addStrategy(String strategyName, ParseNewsStrategy strategy) {
        strategyMapForParsingNews.put(strategyName, strategy);
    }
}