package com.holovenkoml.crawler.application.strategy;

import java.util.Map;

public interface StrategyManager<T> {
    Map<String, T> getStrategyMap();
    void addStrategy(String strategyName, T strategy);
}
