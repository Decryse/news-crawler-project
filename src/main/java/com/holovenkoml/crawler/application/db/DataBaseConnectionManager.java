package com.holovenkoml.crawler.application.db;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

public class DataBaseConnectionManager {

    private DataSource dataSource;

    @Value("?{POSTGRESQLJDBC.url}")
    private String dataBaseUrl;

    @Value("?{POSTGRESQLJDBC.username}")
    private String username;

    @Value("?{POSTGRESQLJDBC.password}")
    private String password;

    public DataSource getDataSource(){
        return dataSource;
    }

    @PostConstruct
    private void createDateSource(){
        HikariConfig cfg = new HikariConfig();
        cfg.setJdbcUrl(dataBaseUrl);
        cfg.setUsername(username);
        cfg.setPassword(password);
        cfg.setMaximumPoolSize(5);
        dataSource = new HikariDataSource(cfg);
    }
}
