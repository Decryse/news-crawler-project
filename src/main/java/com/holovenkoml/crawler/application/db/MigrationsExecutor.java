package com.holovenkoml.crawler.application.db;

import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

@Component
public class MigrationsExecutor {

    private final DataBaseConnectionManager dataBaseConnectionManager;

    @Autowired
    public MigrationsExecutor(DataBaseConnectionManager dataBaseConnectionManager){
        this.dataBaseConnectionManager = dataBaseConnectionManager;
    }

    @PostConstruct
    public void executeMigrations(){
        Flyway flyway = createFlyway(dataBaseConnectionManager.getDataSource());
        flyway.migrate();
    }

    private Flyway createFlyway(DataSource dataSource){
        return Flyway.configure()
                .dataSource(dataSource)
                .load();
    }
}
