package com.holovenkoml.crawler.application.newsparsing;

import com.holovenkoml.crawler.application.domainobject.News;
import com.holovenkoml.crawler.application.printing.Printer;
import com.holovenkoml.crawler.application.repositories.LinksRepository;
import com.holovenkoml.crawler.application.repositories.NewsRepository;
import com.holovenkoml.crawler.application.strategy.StrategyManager;
import com.holovenkoml.crawler.application.utils.ExecutorUtils;
import com.holovenkoml.crawler.websites.template.ParseNewsStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.*;

@Component
public class WebPagesParser implements Parser{

    private final LinksRepository linksRepository;
    private final NewsRepository newsRepository;
    private final StrategyManager<ParseNewsStrategy> strategyManager;
    private final Printer printer;

    @Autowired
    public WebPagesParser(LinksRepository linksRepository,
                          NewsRepository newsRepository,
                          StrategyManager<ParseNewsStrategy> strategyManager,
                          Printer printer){
        this.linksRepository = linksRepository;
        this.newsRepository = newsRepository;
        this.strategyManager = strategyManager;
        this.printer = printer;
    }

    @Override
    public void parse(){
        Set<String> allLinksToTheWebsites = linksRepository.getAllLinksToTheWebsites();
        printer.print("Parsing news...");
        ExecutorService executorService = Executors.newFixedThreadPool(20);
        CompletionService<News> completionService = new ExecutorCompletionService<>(executorService);
        parseNews(allLinksToTheWebsites, completionService);
        executeFutures(allLinksToTheWebsites, completionService);
        ExecutorUtils.shutDownExecutorServiceCorrectly(executorService);
    }

    private void parseNews(Set<String> linksToTheNews, CompletionService<News> completionService){
        linksToTheNews.forEach(link -> {
            Callable<News> task = new ParseWebPageTask(link, strategyManager);
            completionService.submit(task);
        });
    }


    private void executeFutures(Set<String> linksToTheNews, CompletionService<News> completionService){
        for (String link : linksToTheNews) {
            try {
                Future<News> future = completionService.take();
                newsRepository.addParsedNews(future.get());
            } catch (ExecutionException e) {
                printer.print(e.getCause().getMessage());
                linksRepository.putUnparsedLink(link);
            } catch (InterruptedException e) {
                e.printStackTrace();
                Thread.currentThread().interrupt();
            }
        }
    }

}


