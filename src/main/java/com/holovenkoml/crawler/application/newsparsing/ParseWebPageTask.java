package com.holovenkoml.crawler.application.newsparsing;

import com.holovenkoml.crawler.application.domainobject.News;
import com.holovenkoml.crawler.application.exceptions.ParsingException;
import com.holovenkoml.crawler.application.strategy.StrategyManager;
import com.holovenkoml.crawler.websites.template.ParseNewsStrategy;

import java.util.Map;
import java.util.concurrent.Callable;

public class ParseWebPageTask implements Callable<News> {

    private final String url;
    private final StrategyManager<ParseNewsStrategy> strategyManager;

    public ParseWebPageTask(String url, StrategyManager<ParseNewsStrategy> strategyManager){
        this.url = url;
        this.strategyManager = strategyManager;
    }

    @Override
    public News call() {
        Map<String, ParseNewsStrategy> strategyMapForParsingNews = strategyManager.getStrategyMap();
        for(Map.Entry<String, ParseNewsStrategy> entry : strategyMapForParsingNews.entrySet()){
            if(url.startsWith(entry.getKey())){
                return entry.getValue().parse(url);
            }
        }

        throw new ParsingException(url);
    }
}
