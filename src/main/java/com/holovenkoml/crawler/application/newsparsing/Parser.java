package com.holovenkoml.crawler.application.newsparsing;

public interface Parser {
    void parse();
}
